﻿using System;
using System.Globalization;
using Card.IO;
using CardChecker.Model;
using CardChecker.View;

namespace CardChecker.iOS
{
    public class CardIOPaymentViewControllerDg : CardIOPaymentViewControllerDelegate
    {
        private CreditCardEntryView ccPage;
        private CreditCard ccPCL = new CreditCard();

        public CardIOPaymentViewControllerDg(CreditCardEntryView ccEntryPage)
        {
            ccPage = ccEntryPage;
        }

        public override void UserDidCancelPaymentViewController(CardIOPaymentViewController paymentViewController)
        {
            // Console.WriteLine("Scanning Canceled!");
            paymentViewController.DismissViewController(true, null);
            // ccPage.OnScanCancelled();
            Xamarin.Forms.MessagingCenter.Send<CreditCard>(ccPCL, "CreditCardScanCancelled");
        }
        public override void UserDidProvideCreditCardInfo(CreditCardInfo card, CardIOPaymentViewController paymentViewController)
        {
            paymentViewController.DismissViewController(true, null);

            if (card == null)
            {
                Console.WriteLine("Scanning Canceled!");

                //ccPage.OnScanCancelled();
                Xamarin.Forms.MessagingCenter.Send<CreditCard>(ccPCL, "CreditCardScanCancelled");
            }
            else
            {
                //Console.WriteLine("Card Scanned: " + card.CardNumber);

                // Feel free to extend the CreditCard_PCL object to include more than what's here.
                ccPCL.CardNumber = card.CardNumber;
                ccPCL.CVV = card.Cvv;
                ccPCL.ExpirationDate = card.ExpiryMonth.ToString(CultureInfo.InvariantCulture) + card.ExpiryYear;
                ccPCL.RedactedCardNumber = card.RedactedCardNumber;
                ccPCL.CardNetwork = card.CardType.ToString();
                ccPCL.CardHolderName = card.CardholderName;

                //ccPage.OnScanSucceeded (ccPCL);
                Xamarin.Forms.MessagingCenter.Send<CreditCard>(ccPCL, "CreditCardScanSuccess");

            }

        }
    }
}