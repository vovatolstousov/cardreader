﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Card.IO;

namespace CardChecker.Droid
{
    [Activity(Label = "CardChecker", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);


            // Feel free to extend the CreditCard_PCL object to include more than what's here.
            Model.CreditCard ccPCL = new Model.CreditCard();

            if (data != null)
            {

                // Be sure to JavaCast to a CreditCard (normal cast won't work)		 
                var card = data.GetParcelableExtra(CardIOActivity.ExtraScanResult).JavaCast<CreditCard>();

                Console.WriteLine("Scanned: " + card.RedactedCardNumber);

                ccPCL.CardNumber = card.CardNumber;
                ccPCL.CVV = card.Cvv;
                ccPCL.ExpirationDate = card.ExpiryMonth.ToString() + card.ExpiryYear;
                ccPCL.RedactedCardNumber = card.RedactedCardNumber;
                ccPCL.CardNetwork = card.CardType.ToString();
                ccPCL.CardHolderName = card.CardholderName;

                Xamarin.Forms.MessagingCenter.Send<Model.CreditCard>(ccPCL, "CreditCardScanSuccess");

            }
            else
            {
                Xamarin.Forms.MessagingCenter.Send<Model.CreditCard>(ccPCL, "CreditCardScanCancelled");
            }

        }
    }

}

