﻿using System.Windows.Input;
using CardChecker.Command;
using CardChecker.Model;
using CardChecker.View;
using Xamarin.Forms;

namespace CardChecker.ViewModel
{
    public class CreditCardViewModel : BaseViewModel
    {
        public INavigation Navigation { get; set; }
        private CreditCard UserCreditCard { get; set; }
        public CreditCardViewModel()
        {
            MessagingCenter.Subscribe<CreditCard>(this, "CreditCardScanSuccess", (sender) => {
                // Do something whenever the "iOSCreditCardReceived" message is sent.
                // We could fill in CCV and expiration date things here, whatever else we need.
                // This is enough to show capability, however.
                UserCreditCard = sender;
                CvvCode = UserCreditCard.CVV;
                CardNumber = UserCreditCard.RedactedCardNumber;
                ExpirationDate = UserCreditCard.ExpirationDate;
                Navigation.PopModalAsync();
            });

            MessagingCenter.Subscribe<CreditCard>(this, "CreditCardScanCancelled", (sender) => {
                // Do something whenever the "CreditCardCancelled" message is sent.
                Navigation.PopModalAsync();
            });
        }
        private RelayCommand cardReaderCommand;
        public ICommand CardReaderCommand
        {
            get
            {
                return cardReaderCommand ??
                       (cardReaderCommand = new RelayCommand(async () =>
                       {
                           CardIOConfig cardIOConfig = new CardIOConfig();
                           var ccPage = new CreditCardEntryView(cardIOConfig);
                           await Navigation.PushModalAsync(ccPage);
                       }));
            }
        }

        private string expirationDate;
        private string NameExpirationDateProperty = "ExpirationDate";
        public string ExpirationDate
        {
            get { return expirationDate; }
            set
            {
                if (expirationDate != value)
                {
                    SetProperty(ref expirationDate, value, NameExpirationDateProperty);
                }
            }
        }

        private string cardNumber;
        private string NameCardNumberProperty = "CardNumber";
        public string CardNumber
        {
            get { return cardNumber; }
            set
            {
                if (cardNumber != value)
                {
                    SetProperty(ref cardNumber, value, NameCardNumberProperty);
                }
            }
        }

        private string cvvCode;
        private string NameCvvCodeProperty = "CvvCode";
        public string CvvCode
        {
            get { return cvvCode; }
            set
            {
                if (cvvCode != value)
                {
                    SetProperty(ref cvvCode, value, NameCvvCodeProperty);
                }
            }
        }


    }
}