﻿using System;
using CardChecker.Model;
using Xamarin.Forms;

namespace CardChecker.View
{

    public class CreditCardEntryView : ContentPage
    {
        public delegate void ScanSucceededEventHandler(object sender, CreditCard ccPCL);

        public event ScanSucceededEventHandler ScanSucceeded;
        public event EventHandler ScanCancelled;

        public CardIOConfig cardIOConfig;

        public CreditCardEntryView(CardIOConfig config)
        {
            cardIOConfig = config;
        }

        public void OnScanSucceeded(CreditCard ccPCL)
        {
            ScanSucceeded?.Invoke(this, ccPCL);
        }

        public void OnScanCancelled()
        {
            ScanCancelled?.Invoke(this, EventArgs.Empty);
        }
    }
}