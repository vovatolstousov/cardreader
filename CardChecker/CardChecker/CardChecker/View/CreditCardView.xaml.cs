﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardChecker.ViewModel;
using Xamarin.Forms;

namespace CardChecker.View
{
    public partial class CreditCardView
    {
        public CreditCardViewModel ViewModel
        {
            get { return BindingContext as CreditCardViewModel; }
            set { BindingContext = value; }
        }
        public CreditCardView()
        {
            InitializeComponent();
            ViewModel = new CreditCardViewModel() { Navigation = this.Navigation };
        }
    }
}
