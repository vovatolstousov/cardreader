﻿namespace CardChecker.Model
{
    public class CreditCard  
    {
       
        public string CardNetwork { get; set; }
        public string CVV { get; set; }
        public string ExpirationDate { get; set; }
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public string RedactedCardNumber { get; set; }
    }
}